package linkedlist;

public class LinkedList {
    Node head;

    public LinkedList() {
        head = null;
    }

    public void insertNode(int val) {
        Node node = new Node(val);
        if (head == null) {
            head = node;
        } else {
            Node temp = head;

            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
    }

    public void insertNode(Node node) {
        if (head == null) {
            head = node;
        } else {
            Node temp = head;

            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
    }

    public void print() {
        Node temp = head;

        while (temp != null) {
            System.out.print(temp.data + " ");
            temp = temp.next;
        }
        System.out.println();
    }

    public void sortedInsert(int val) {

        Node newNode = new Node(val);
        if (head == null || head.data > newNode.data) {
            newNode.next = head;
            head = newNode;
        } else {
            Node current = head;

            while (current.next != null && current.next.data < newNode.data) {
                current = current.next;
            }
            newNode.next = current.next;
            current.next = newNode;

        }

    }

    public void deleteNode(int val) {
        if (head == null) return;

        Node current = head;

        while (current != null ){
            if (current == head && current.data == val){
                deleteHead();
                current = head;
            } else {
            }
        }
    }

    public void deleteHead() {
        Node current = head;
        head = head.next;
        current.next = null;
    }


}
