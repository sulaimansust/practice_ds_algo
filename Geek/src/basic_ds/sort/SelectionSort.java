package basic_ds.sort;

/**
 * Created by sulaimankhan on 12/24/2017.
 * Complexity O(n^2)
 * Always find the minimum value and insert from the beginning like 1st lowest number in position 0 second lowest in position 1 and so on
 */
public class SelectionSort {

    public static void sort(int[] data) {

        for (int i = 0; i < data.length; i++) {

            int min = data[i], position = i;
            for (int j = i; j < data.length; j++) {
                if (min > data[j]) {
                    min = data[j];
                    position = j;
                }
            }
            int temp = data[i];
            data[i] = min;
            data[position] = temp;
        }
    }
}
