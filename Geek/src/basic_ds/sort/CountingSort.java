package basic_ds.sort;

/**
 * Created by sulaimankhan on 12/25/2017.
 * complexity O(n)
 */
public class CountingSort {

    public static void sort(int[] data, int max) {
        int[] countArray = new int[max + 1];

        for (int i = 0; i < data.length; i++) {
            countArray[data[i]]++;
        }
        int start = 0;
        for (int i = 0; i < max + 1; i++) {
            for (int j = 0; j < countArray[i]; j++) {
                data[start++] = i;
            }
        }

    }
}
