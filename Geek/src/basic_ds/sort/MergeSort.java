package basic_ds.sort;

/**
 * Created by sulaimankhan on 12/25/2017.
 */
public class MergeSort {

    public static void sort(int[] data, int start, int end) {
        if (start < end) {
            int mid = (start + end) / 2;
            sort(data, start, mid);
            sort(data, mid + 1, end);
            merge(data, start, mid, end);
        }
    }

    private static void merge(int[] data, int start, int mid, int end) {

        int sizeOfLeftArray = mid - start + 1;
        int sizeOfRightArray = end - mid;

        int[] leftArray = new int[sizeOfLeftArray];
        int[] rightArray = new int[sizeOfRightArray];

        for (int i = 0; i < sizeOfLeftArray; i++) {
            leftArray[i] = data[start + i];
        }

        for (int i = 0; i < sizeOfRightArray; i++) {
            rightArray[i] = data[mid + i + 1];
        }
        int left = 0, right = 0;
        int k = start;

        while (left < sizeOfLeftArray && right < sizeOfRightArray) {
            if (leftArray[left] < rightArray[right]) {
                data[k++] = leftArray[left++];
            } else {
                data[k++] = rightArray[right++];
            }
        }
        while (left<sizeOfLeftArray){
            data[k++] = leftArray[left++];
        }
        while (right<sizeOfRightArray){
            data[k++] = rightArray[right++];
        }

    }
}
