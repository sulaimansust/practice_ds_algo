package basic_ds.sort;

/**
 * Created by sulaimankhan on 12/25/2017.
 */
public class QuickSort {

    public static void sort(int[] data, int low, int high) {
        if (low < high) {
            int partitionIndex = partition(data, low, high);
            sort(data, low, partitionIndex - 1);
            sort(data, partitionIndex + 1, high);
        }
    }

    private static int partition(int[] data, int low, int high) {

        int pivot = data[high];
        int i = low - 1;

        for (int j = low; j < high; j++) {
            if (data[j] <= pivot) {
                i++;
                int temp = data[j];
                data[j] = data[i];
                data[i] = temp;
            }
        }

        int temp = data[i + 1];
        data[i + 1] = data[high];
        data[high] = temp;

        return i + 1;
    }
}
