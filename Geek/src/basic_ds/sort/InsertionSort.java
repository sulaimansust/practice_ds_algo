package basic_ds.sort;

/**
 * Created by sulaimankhan on 12/24/2017.
 * InsertionSort Complexity O(n^2)
 */
public class InsertionSort {

    public static void sort(int[] data) {
        int j, key;

        for (int i = 0; i < data.length; i++) {
            key = data[i];
            j = i - 1;

            while (j >= 0 && data[j] > key) {
                data[j + 1] = data[j];
                j--;
            }
            data[j + 1] = key;
        }
    }


    public static void insertionSort(int[] data) {
        int key, j;

        for (int i = 0; i < data.length; i++) {
            key = data[i];
            j = i - 1;
            while (j >= 0 && data[j] > key) {
                data[j + 1] = data[j];
                j--;
            }
            data[j + 1] = key;
        }
    }


}
