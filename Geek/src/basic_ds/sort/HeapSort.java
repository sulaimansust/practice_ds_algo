package basic_ds.sort;

/**
 * Created by sulaimankhan on 12/25/2017.
 */
public class HeapSort {

    public static void sort(int data[]) {

        // First build heap by rearranging array
        for (int i = data.length / 2 - 1; i >= 0; i--) {
            heapify(data, data.length, i);
        }

        // Now one by one extract one element from heap

        for (int i = data.length - 1; i >= 0; i--) {
            // Move current root to end
            int temp = data[0];
            data[0] = data[i];
            data[i] = temp;

            // call max heapify on the reduced heap
            heapify(data, i, 0);
        }


    }

    // To heapify a subtree rooted with node i which is
    // an index in arr[]. n is size of heap or array
    private static void heapify(int data[], int n, int i) {

        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        //if left child is larger than root
        if (left < n && data[left] > data[largest]) {
            largest = left;
        }

        //If right child is larger than root
        if (right < n && data[right] > data[largest]) {
            largest = right;
        }

        // If largest is not root
        if (largest != i) {
            int temp = data[largest];
            data[largest] = data[i];
            data[i] = temp;

            // Recursively heapify the affected sub-tree
            heapify(data, n, largest);
        }


    }

}
