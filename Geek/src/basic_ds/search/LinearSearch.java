package basic_ds.search;

/**
 * Created by sulaimankhan on 12/22/2017.
 */
public class LinearSearch {

    public static int linearSearch(int[] input, int key) {

        for (int i = 0; i < input.length; i++) {
            if (input[i] == key)
                return i;
        }
        return -1;
    }

}
