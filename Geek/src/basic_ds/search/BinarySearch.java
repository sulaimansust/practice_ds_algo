package basic_ds.search;

/**
 * Created by sulaimankhan on 12/22/2017.
 */
public class BinarySearch {
    public static int binarySearch(int[] data, int left, int right, int key) {

        if (right > left) {
            int mid = (left + right) / 2;
            if (data[mid] == key) {
                return mid;
            }
            if (data[mid] > key)
                return binarySearch(data, left, mid - 1, key);
            else
                return binarySearch(data, mid + 1, right, key);
        }


        return -1;
    }

    public static void search(int[] data, int key) {
        System.out.println();
    }
}
