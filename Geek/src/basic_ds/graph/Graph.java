package basic_ds.graph;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

public class Graph {
    private int noOfVertex;
    private LinkedList<Integer> adjacencyList[];

    public Graph(int noOfVertex) {
        this.noOfVertex = noOfVertex;
        adjacencyList = new LinkedList[noOfVertex];

        for (int i = 0; i < noOfVertex; i++) {
            adjacencyList[i] = new LinkedList<>();
        }

    }

    public void addEdge(int vertex, int edge) {
        adjacencyList[vertex].add(edge);
    }

    public void bfsTraverse(int startNode) {

        boolean visited[] = new boolean[noOfVertex];

        LinkedList<Integer> queue = new LinkedList<>();

        visited[startNode] = true;
        queue.add(startNode);

        while (queue.size() != 0) {
            startNode = queue.poll();

            System.out.print(startNode + " ");

            Iterator<Integer> iterator = adjacencyList[startNode].listIterator();

            while (iterator.hasNext()) {
                int n = iterator.next();

                if (!visited[n]) {
                    visited[n] = true;
                    queue.add(n);
                }
            }


        }

    }

    private void dfsUtils(int node, boolean visited[]) {
        visited[node] = true;
        System.out.print(node + " ");

        Iterator<Integer> iterator = adjacencyList[node].listIterator();

        while (iterator.hasNext()) {
            int n = iterator.next();
            if (!visited[n]) {
                dfsUtils(n, visited);
            }
        }


    }

    /*
    *
    * This one traverse only connected graph
    *
    * */
    public void dfsTraversal(int node) {
        boolean visited[] = new boolean[noOfVertex];

//        This one traverse only connected graph
//        dfsUtils(node, visited);

        //This solution for all disconnected graph

        for (int i=0;i<noOfVertex;i++){
            if (!visited[i]){
                dfsUtils(i,visited);
            }
        }

    }


    private void topologicalSortUtils(int node, boolean visited[], Stack stack) {
        visited[node] = true;
        Integer i;

        Iterator<Integer> iterator = adjacencyList[node].listIterator();

        while (iterator.hasNext()) {
            i = iterator.next();
            if (!visited[i]) {
                topologicalSortUtils(i, visited, stack);
            }
        }
        stack.push(new Integer(node));

    }

    public void topologicalSort() {
        Stack stack = new Stack();

        boolean[] visited = new boolean[noOfVertex];


        for (int i = 0; i < noOfVertex; i++) {
            if (!visited[i]) {
                topologicalSortUtils(i, visited, stack);
            }
        }

        while (!stack.empty())
            System.out.print(stack.pop() + " ");

    }

}
