package arrays.ArrayRotation;

public class TwoSumOnRotatedArray {


    public int findPivot(int data[], int low, int high) {

        if (high < low)
            return -1;

        if (high == low)
            return low;

        int mid = (low + high) / 2;

        if (mid < high && data[mid] > data[mid + 1])
            return mid;

        if (low < mid && data[mid] < data[mid - 1])
            return mid - 1;

        if (data[low] >= data[mid])
            return findPivot(data, low, mid - 1);
        return findPivot(data, mid + 1, high);

    }

    public boolean twoSum(int data[], int sum) {
        int pivot = findPivot(data, 0, data.length);
        int start, end;

        if (pivot == -1) {
            start = 0;
            end = data.length - 1;
        } else {
            start = pivot + 1;
            end = pivot;
        }

        for (int i = 0; i < data.length; i++) {
            if (end < 0 || start > data.length - 1)
                return false;
            int currentSum = data[start] + data[end];
            if (sum == currentSum) {
                System.out.println("start : " + start + "end: " + end);
                return true;
            } else if (currentSum < sum) {
                start++;
            } else {
                end--;
            }
        }

        return false;

    }
}
