package arrays.ArrayRotation;

public class SearchRotatedArray {

    public int findPivot(int[] data, int low, int high) {
        if (high < low)
            return -1;

        if (high == low)
            return low;

        int mid = (low + high) / 2;
        if (mid < high && data[mid] > data[mid + 1])
            return mid;

        if (mid > low && data[mid] < data[mid - 1])
            return mid - 1;

        if (data[low] >= data[mid])
            return findPivot(data, low, mid - 1);

        return findPivot(data, mid + 1, high);


    }

    public int findPivotN(int data[]) {

        for (int i = 1; i < data.length; i++) {
            if (data[i] < data[i - 1])
                return i;
        }

        return -1;
    }

    public int binarySearch(int data[], int low, int high, int key) {
        if (high < low)
            return -1;

        int mid = (low + high) / 2;

        if (key == data[mid])
            return mid;
        if (key > data[mid])
            return binarySearch(data, mid + 1, high, key);
        return binarySearch(data, low, mid - 1, key);

    }

    public void binarySearchOnRotatedArray(int[] array, int key) {
        //this one find in n complexity
        int pivot;
        pivot = findPivotN(array);

        //this one find on logN
        pivot = findPivot(array, 0, array.length);

        int index;
        if (pivot == -1) {
            index = binarySearch(array, 0, array.length - 1, key);
        }
        if (array[0] > key) {
            index = binarySearch(array, pivot + 1, array.length, key);
        } else
            index = binarySearch(array, 0, pivot, key);

        if (index != -1)
            System.out.println("data found in " + index);
        else
            System.out.println("no data found");
    }


}
