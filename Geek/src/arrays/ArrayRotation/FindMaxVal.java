package arrays.ArrayRotation;

/*
*
*Find maximum value of Sum( i*arr[i]) with only rotations on given array allowed
*
* */
public class FindMaxVal {

    public long maxSum(int data[]) {
        long sum = sum(data);
        RotateArray rotateArray = new RotateArray();
        for (int i = 0; i < data.length; i++) {
            rotateArray.rotateRight(data, 1);
            long newSum = sum(data);
            if (sum < newSum) {
                sum = newSum;
            }
        }
        return sum;
    }

    private long sum(int data[]) {

        long sum = 0;

        for (int i = 0; i < data.length; i++) {
            sum += i * data[i];
        }
        return sum;
    }

    public long maxSumEfficient(int[] data) {

        int arrSum = 0;
        int currVal = 0;

        for (int i = 0; i < data.length; i++) {
            arrSum += data[i];
            currVal += (i * data[i]);
        }

        int maxVal = currVal;


        for (int j = 1; j < data.length; j++) {
            currVal = currVal + arrSum - data.length * data[data.length - j];

            if (currVal > maxVal)
                maxVal = currVal;

        }
        return maxVal;

    }


}
