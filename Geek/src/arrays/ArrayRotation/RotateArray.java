package arrays.ArrayRotation;

public class RotateArray {

    public void rotateLeft(int[] input, int num) {
        int tempArray[] = new int[num];

        for (int i = 0; i < num; i++) {
            tempArray[i] = input[i];
        }
        int start = 0;
        for (int i = num; i < input.length; i++) {
            input[i - num] = input[i];
        }
        for (int i = 0; i < num; i++) {
            input[input.length - num + i] = tempArray[i];
        }

//        System.out.println("rotateLeft");
//        printArray(input);

    }

    public void rotateRight(int[] input, int num) {
        int tempArray[] = new int[num];
        int start = 0;
        for (int i = input.length - num; i < input.length; i++) {
//            System.out.println(i);
            tempArray[start++] = input[i];
        }
//        System.out.println();
//        System.out.println();
        int lastPosition = input.length - 1;
        for (int i = input.length - num - 1; i >= 0; i--) {
            input[lastPosition--] = input[i];
        }
        for (int i = 0; i < num; i++) {
            input[i] = tempArray[i];
        }

//        System.out.println("rotateRight");
//        printArray(input);

    }

    public static void printArray(int[] input) {
        for (int i = 0; i < input.length; i++) {
            System.out.println(input[i]);
        }
    }

    public static void main(String[] args) {
        RotateArray rotateArray = new RotateArray();
        int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        rotateArray.rotateRight(array, 3);

        System.out.println("after rotate");
        printArray(array);

        SearchRotatedArray searchRotatedArray = new SearchRotatedArray();
        searchRotatedArray.binarySearchOnRotatedArray(array, 3);

        TwoSumOnRotatedArray twoSumOnRotatedArray = new TwoSumOnRotatedArray();

        System.out.println("two sum on roateted array");
        System.out.println(twoSumOnRotatedArray.twoSum(array,15));

    }
}
