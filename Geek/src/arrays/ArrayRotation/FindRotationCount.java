package arrays.ArrayRotation;

/*
*
*  Rotation count for anti clockwise rotation
*
* */
public class FindRotationCount {

    public int findPivot(int data[], int low, int high) {

        if (high < low)
            return -1;

        if (high == low)
            return low;

        int mid = (high + low) / 2;

        if (mid < high && data[mid] > data[mid + 1])
            return mid;

        if (low < mid && data[mid] < data[mid - 1])
            return mid - 1;

        if (data[low] >= data[mid])
            return findPivot(data, low, mid - 1);

        return findPivot(data, mid + 1, high);
    }

    public int rotationCount(int data[]) {
        System.out.println(findPivot(data, 0, data.length - 1));

        return 0;
    }
}
