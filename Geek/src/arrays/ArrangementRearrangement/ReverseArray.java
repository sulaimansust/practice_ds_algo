package arrays.ArrangementRearrangement;

public class ReverseArray {

    public static void reverse(int data[]) {
        int start = 0, end = data.length - 1;

        while (start < end) {
            int temp = data[start];
            data[start] = data[end];
            data[end] = temp;
            start++;
            end--;
        }

    }
}
