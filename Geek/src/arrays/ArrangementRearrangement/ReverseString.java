package arrays.ArrangementRearrangement;

public class ReverseString {
    public static String reverse(String data) {

        char[] array = data.toCharArray();


        int start = 0, end = array.length-1;

        while (start<end){
            char temp = array[start];
            array[start]  = array[end];
            array[end] = temp;
            start++;
            end--;
        }


        return new String(array);
    }
}
