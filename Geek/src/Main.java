package arrays;
import arrays.ArrangementRearrangement.ReverseArray;
import arrays.ArrangementRearrangement.ReverseString;
import arrays.ArrayRotation.FindMaxVal;
import basic_ds.graph.Graph;
import linkedlist.LinkedList;

public class Main {
    public static void main(String[] args) {
        int array[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        int array2[] = {1, 20, 2, 10};
        int array3[] = {6, 7, 8, 9, 10, 1, 2, 3, 4, 5,};

        FindMaxVal maxVal = new FindMaxVal();

//        System.out.println(maxVal.maxSum(array3));

//        FindRotationCount findRotationCount = new FindRotationCount();
//        findRotationCount.rotationCount(array3);

        ReverseArray.reverse(array);
        printArray(array);

        System.out.println(ReverseString.reverse("Hello"));

        System.out.println("=====");
//        InsertionSort.insertionSort(array);
//        SelectionSort.sort(array);
//        BubbleSort.sortLowIteration(array);
//        CountingSort.sort(array, 11);

//        QuickSort.sort(array,0,array.length-1);

//        MergeSort.sort(array,0,array.length-1);

//        HeapSort.sort(array);

//        printArray(array);


//        createGraphAndTraverse();

//        createAnotherGraph();

        createLinkList();

    }

    private static void createLinkList(){
        LinkedList linkedList = new LinkedList();
        linkedList.sortedInsert(6);
        linkedList.sortedInsert(7);
        linkedList.sortedInsert(1);
        linkedList.sortedInsert(9);
        linkedList.sortedInsert(4);
        linkedList.sortedInsert(5);
        linkedList.sortedInsert(2);
        linkedList.sortedInsert(3);
        linkedList.sortedInsert(8);

        linkedList.sortedInsert(10);


        //Task
        linkedList.print();





    }

    private static void createGraphAndTraverse() {
        Graph g = new Graph(4);

        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        g.bfsTraverse(2);
        System.out.println("\n " + "DFS traversal starting");
        g.dfsTraversal(2);

    }

    private static void createAnotherGraph() {
        Graph g = new Graph(6);
        g.addEdge(5, 2);
        g.addEdge(5, 0);
        g.addEdge(4, 0);
        g.addEdge(4, 1);
        g.addEdge(2, 3);
        g.addEdge(3, 1);

        g.topologicalSort();
    }

    public static void printArray(int[] input) {
        for (int i = 0; i < input.length; i++) {
            System.out.print(input[i] + " ");
        }
        System.out.println();
    }
}
